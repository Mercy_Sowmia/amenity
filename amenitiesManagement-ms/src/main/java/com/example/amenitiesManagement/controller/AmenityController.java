package com.example.amenitiesManagement.controller;

import com.example.amenitiesManagement.constantAndEnum.AmenityTypeEnum;
import com.example.amenitiesManagement.constantAndEnum.GenderEnum;
import com.example.amenitiesManagement.constantAndEnum.TimeEnum;
import com.example.amenitiesManagement.exceptions.AgeReservationException;
import com.example.amenitiesManagement.exceptions.NameLengthException;
import com.example.amenitiesManagement.model.request.AmenityRequest;
import com.example.amenitiesManagement.service.AmenityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.Date;

@RestController
@RequestMapping("/management")
public class AmenityController {

    @Autowired
    AmenityService amenitiesService;

/*
    @PostMapping("/add")
    public ResponseEntity<String> setManagement(@Valid AmenityRequest amenitiesRequest){
        return new ResponseEntity(amenitiesService.create(amenitiesRequest), HttpStatus.OK);
    }
*/

    @PostMapping("/create")
    public ResponseEntity<?> addExperience(@RequestParam String name, @RequestParam AmenityTypeEnum amenityTypeEnum,
                                                @RequestParam Date date, @RequestParam TimeEnum timeEnum,
                                                @RequestParam GenderEnum genderEnum, @RequestParam Integer age)
    {
        try {
            return new ResponseEntity(amenitiesService.create(name, amenityTypeEnum,
                    date, timeEnum, genderEnum, age), HttpStatus.OK);
        } catch (NameLengthException l) {
            return new ResponseEntity<>(l.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (AgeReservationException l) {
            return new ResponseEntity<>(l.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception l) {
            return new ResponseEntity<>(l.getMessage(), HttpStatus.BAD_REQUEST);
        }
        }

    @GetMapping("/get")
    public ResponseEntity<?> getManagement(@RequestParam String id){
        return new ResponseEntity(amenitiesService.get(id),HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateManagement(@RequestParam String id , @RequestParam String name,
                                               @RequestParam AmenityTypeEnum amenitiesType, @RequestParam Date date,
                                               @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender,
                                               @RequestParam Integer age){
        try {
            return new ResponseEntity(amenitiesService.update(id, name, amenitiesType, date, amenityTime, gender, age),
                    HttpStatus.OK);
        }catch(NameLengthException l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
        catch(Exception l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteManagement(@RequestParam String id){
        return new ResponseEntity(amenitiesService.delete(id),HttpStatus.OK);
    }

}
