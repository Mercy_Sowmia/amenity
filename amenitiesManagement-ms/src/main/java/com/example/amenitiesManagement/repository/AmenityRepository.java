package com.example.amenitiesManagement.repository;

import com.example.amenitiesManagement.model.entity.AmenityEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AmenityRepository extends MongoRepository<AmenityEntity, String> {

}
