package com.example.amenitiesManagement.constantAndEnum;

import java.util.Arrays;
import java.util.List;

public class AmenityTime {

    public static final List<String> SCHEDULE_ONE = Arrays.asList("9AM-10AM", "10AM-11AM", "11AM-12PM",
            "12PM-1PM", "1PM-2PM", "2PM-3PM", "3PM-4PM", "4PM-5PM");
    public static final List<String> SCHEDULE_TWO = Arrays.asList("5PM-6PM", "6PM-7PM", "7PM-8PM",
            "8PM-9PM");

}
