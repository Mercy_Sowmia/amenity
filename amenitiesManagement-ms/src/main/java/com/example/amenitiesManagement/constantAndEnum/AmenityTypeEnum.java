package com.example.amenitiesManagement.constantAndEnum;

public enum AmenityTypeEnum {

    GYM("GYM"), SWIMMING_POOL("SWIMMING POOL"), STEAM_BATH("STEAM BATH"),  YOGA("YOGA");

    private String amenityTypes;
    AmenityTypeEnum(final String amenity){
        amenityTypes = amenity;
    }

    public String getAmenityTypes(){
        return amenityTypes;
    }
}
