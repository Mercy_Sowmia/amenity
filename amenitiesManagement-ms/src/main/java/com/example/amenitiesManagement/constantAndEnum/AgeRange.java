package com.example.amenitiesManagement.constantAndEnum;

public final class AgeRange {
    public static final int MIN_GYM_AGE =18;
    public static final int MAX_GYM_AGE =60;
    public static final int MIN_SWIMMING_POOL_AGE =12;
    public static final int MAX_SWIMMING_POOL_AGE =60;
    public static final int MIN_STEAM_BATH_AGE =18;
    public static final int MAX_STEAM_BATH_AGE =60;
    public static final int MIN_YOGA_AGE =10;
    public static final int MAX_YOGA_AGE =60;

}
