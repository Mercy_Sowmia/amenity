package com.example.amenitiesManagement.exceptions;

public class WrongSelectionException extends RuntimeException{
    public WrongSelectionException(){
        super("Wrong Selection of Amenity");
    }
}
