package com.example.amenitiesManagement.service;

import com.example.amenitiesManagement.constantAndEnum.AmenityTypeEnum;
import com.example.amenitiesManagement.constantAndEnum.GenderEnum;
import com.example.amenitiesManagement.constantAndEnum.TimeEnum;
import com.example.amenitiesManagement.exceptions.AgeReservationException;
import com.example.amenitiesManagement.exceptions.NameLengthException;
import com.example.amenitiesManagement.exceptions.ReservationFullException;
import com.example.amenitiesManagement.model.entity.AmenityEntity;
import com.example.amenitiesManagement.model.request.AmenityRequest;
import com.example.amenitiesManagement.model.response.AmenityResponse;
import com.example.amenitiesManagement.repository.AmenityRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

import static com.example.amenitiesManagement.constantAndEnum.AgeRange.*;
import static com.example.amenitiesManagement.constantAndEnum.AmenityPrice.*;
import static com.example.amenitiesManagement.constantAndEnum.AmenityTime.SCHEDULE_ONE;
import static com.example.amenitiesManagement.constantAndEnum.AmenityTime.SCHEDULE_TWO;

@Service
@Slf4j
public class AmenityService {
    @Autowired
    AmenityRepository amenitiesRepository;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    MongoTemplate mongoTemplate;


    //PostProcess
    public Object create(String name, AmenityTypeEnum amenityTypeEnum, Date date,
                         TimeEnum timeEnum, GenderEnum genderEnum, Integer age) {
        AmenityRequest amenityRequest = new AmenityRequest();
        amenityRequest.setName(name);
        amenityRequest.setAmenitiesTypes(amenityTypeEnum);
        amenityRequest.setDate(date);
        amenityRequest.setAmenityTime(timeEnum);
        amenityRequest.setUserGender(genderEnum);
        amenityRequest.setAge(age);
        AmenityResponse amenityResponse = new AmenityResponse();

        String checkAmenity = amenityRequest.getAmenitiesTypes().getAmenityTypes();
        Date checkDate = amenityRequest.getDate();
        String checkTime = amenityRequest.getAmenityTime().getAmenityTime();
        String[] reservationCheckResult = getReservationDetail(checkAmenity,checkTime);
        int reservationLimit = Integer.parseInt(reservationCheckResult[0]);
        String reservationFee = reservationCheckResult[1];

        if(!nameCheck(amenityRequest.getName())){
            throw new NameLengthException();
        }

        List<AmenityEntity> amenitiesEntities =  getReservationQuery(checkAmenity, checkDate, checkTime);
        if (amenitiesEntities.size() < reservationLimit && ageCheck(amenityRequest.getAmenitiesTypes(),
                amenityRequest.getAge())) {
            AmenityEntity newAmenitiesEntity = modelMapper.map(amenityRequest, AmenityEntity.class);
            amenitiesRepository.save(newAmenitiesEntity);
            amenityResponse = modelMapper.map(newAmenitiesEntity, AmenityResponse.class);
        }
        else{
            throw new ReservationFullException();
        }
        return "Reservation created for " + amenityResponse.getAmenitiesTypes() +
                "\n" +"User ID: "+ amenityResponse.getId() + "\n" + "Fee for " + checkAmenity + " = " + reservationFee;
    }

    //QueryProcess
    private List getReservationQuery(String checkAmenity, Date checkDate, String checkTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("amenitiesTypes").is(checkAmenity));
        query.addCriteria(Criteria.where("date").is(checkDate));
        query.addCriteria(Criteria.where("amenityTime").is(checkTime));
        List<AmenityEntity> users = mongoTemplate.find(query, AmenityEntity.class);
        return users;
    }

    public String[] getReservationDetail(String amenity, String time) {
        String[] reservationDetail = new String[2];
        switch (amenity) {
            case "GYM":
                reservationDetail[0] = "10";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = GYM_SCHEDULE_ONE_FEE;
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = GYM_SCHEDULE_TWO_FEE;
                }
                break;
            case "SWIMMING POOL":
                reservationDetail[0] = "5";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = SWIMMING_POOL_SCHEDULE_ONE_FEE;
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = SWIMMING_POOL_SCHEDULE_TWO_FEE;
                }
                break;
            case "STEAM BATH":
                reservationDetail[0] = "2";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = STEAM_BATH_SCHEDULE_ONE_FEE;
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = STEAM_BATH_SCHEDULE_TWO_FEE;
                }
                break;
            case "YOGA":
                reservationDetail[0] = "3";
                reservationDetail[1] = YOGA_SCHEDULE_FEE;
                break;
            default:
                reservationDetail[0] = "null";
                reservationDetail[1] = "null";
        }
        return reservationDetail;
    }

    //PutProcess
    public String update(String id, String name, AmenityTypeEnum amenitiesType, Date date, TimeEnum amenityTime,
                         GenderEnum userGender, Integer age) {
        AmenityResponse amenitiesResponse = new AmenityResponse();
        AmenityEntity amenitiesEntity = amenitiesRepository.findById(id).orElseThrow(() ->
                new RuntimeException("ID NOT FOUND ERROR"));
        String returnAmenity = "";
        String[] reservationCheckResult = new String[2];

        if (amenitiesEntity != null) {
            String updateUserName = (StringUtils.isNotBlank(name) ? name : amenitiesEntity.getName());
            amenitiesEntity.setName(updateUserName);
            String updateUserAmenity = (StringUtils.isNotBlank(String.valueOf(amenitiesType))) ?
                    (amenitiesType.getAmenityTypes()) : (amenitiesEntity.getAmenitiesTypes());
            amenitiesEntity.setAmenitiesTypes(updateUserAmenity);
            Date updateUserDate = (StringUtils.isNotBlank(String.valueOf(date))) ? date : (Date) amenitiesEntity.getDate();
            amenitiesEntity.setDate(updateUserDate);
            String updateUserTime = (StringUtils.isNotBlank(String.valueOf(amenityTime))) ? (amenityTime.getAmenityTime()) :
                    (amenitiesEntity.getAmenityTime());
            amenitiesEntity.setAmenityTime(updateUserTime);
            String updateUserGender = (StringUtils.isNotBlank(String.valueOf(userGender)) ? (userGender.getUserGender()) :
                    amenitiesEntity.getUserGender());
            amenitiesEntity.setUserGender(updateUserGender);
            Integer updateUserAge = (StringUtils.isNotBlank(String.valueOf(age))) ? (age) : (amenitiesEntity.getAge());
            amenitiesEntity.setAge(updateUserAge);
            reservationCheckResult = getReservationDetail(updateUserAmenity, updateUserTime);

            if(!nameCheck(updateUserName)){
                throw new NameLengthException();
            }

            List<AmenityEntity> amenityEntities = getReservationQuery(amenitiesType.getAmenityTypes(), date, amenityTime.getAmenityTime());
            if ((amenitiesType.getAmenityTypes() == updateUserAmenity) || amenityEntities.size()
                    < Integer.parseInt(reservationCheckResult[0]) && ageCheck(amenitiesType, age)) {
                AmenityEntity newAmenityEntity = modelMapper.map(amenitiesEntity, AmenityEntity.class);
                amenitiesRepository.save(newAmenityEntity);
                amenitiesResponse = modelMapper.map(newAmenityEntity, AmenityResponse.class);
            } else {
                throw new ReservationFullException();
            }
        }
        else
        {
        }

        return "Reservation updated for " +  amenitiesResponse.getAmenitiesTypes() + "\n" + "Fee for " + amenitiesType
                + " = " + reservationCheckResult[1];
    }

    //GetProcess
    public AmenityResponse get(String id) {
        AmenityEntity amenitiesEntity = amenitiesRepository.findById(id).orElseThrow(() -> new RuntimeException("ID not found"));
        return modelMapper.map(amenitiesEntity, AmenityResponse.class);
    }

    //DeleteProcess
        public String delete(String id) {
        amenitiesRepository.deleteById(id);
        return "Deleted";
    }

    //Age Check
    public boolean ageCheck(AmenityTypeEnum amenityTypeEnum, int userAge){
        boolean criteriaResult = false;
        if(amenityTypeEnum.equals(AmenityTypeEnum.GYM)){
            if(userAge>=MIN_GYM_AGE && userAge<=MAX_GYM_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(amenityTypeEnum.equals(AmenityTypeEnum.SWIMMING_POOL)){
            if(userAge>=MIN_SWIMMING_POOL_AGE && userAge<=MAX_SWIMMING_POOL_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(amenityTypeEnum.equals(AmenityTypeEnum.STEAM_BATH)){
            if(userAge>=MIN_STEAM_BATH_AGE && userAge<=MAX_STEAM_BATH_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(amenityTypeEnum.equals(AmenityTypeEnum.YOGA)){
            if(userAge>=MIN_YOGA_AGE && userAge<=MAX_YOGA_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }

        return criteriaResult;
    }


    //Check the UserName Length
    public boolean nameCheck(String name){
        boolean criteriaResult = false;
        if(name.length()>=3 && name.length()<=20){
            criteriaResult =true;
        }
        return criteriaResult;
    }



}



/*
public String create(AmenityRequest amenitiesRequest) {
        AmenityResponse amenitiesResponse = new AmenityResponse();
        AmenityTypeEnum checkAmenity = amenitiesRequest.getAmenitiesTypes();

        AmenityUpdateRequest amenitiesUpdateRequest = new AmenityUpdateRequest();
        amenitiesUpdateRequest.setAmenitiesTypes(amenitiesRequest.getAmenitiesTypes().getAmenityTypes());
        amenitiesUpdateRequest.setAmenityTime(amenitiesRequest.getAmenityTime().getAmenityTime());
        amenitiesUpdateRequest.setAge(amenitiesRequest.getAge());
        amenitiesUpdateRequest.setDate(amenitiesRequest.getDate());
        amenitiesUpdateRequest.setName(amenitiesRequest.getName());
        amenitiesUpdateRequest.setUserGender(amenitiesRequest.getUserGender().getUserGender());

        String amenity = checkAmenity.getAmenityTypes();
        Date checkDate =  amenitiesRequest.getDate();
        TimeEnum checkTime = amenitiesRequest.getAmenityTime();
        String time = checkTime.getAmenityTime();
        String[] reservationCheckResult = getReservationDetail(amenity, time);
        int reservationLimit = Integer.parseInt(reservationCheckResult[0]);
        String reservationFee = reservationCheckResult[1];

        List<AmenityEntity> amenitiesEntities =  getReservationQuery(amenity, checkDate, time);
        if (amenitiesEntities.size() < reservationLimit) {
            AmenityEntity newAmenitiesEntity = modelMapper.map(amenitiesUpdateRequest, AmenityEntity.class);
            amenitiesRepository.save(newAmenitiesEntity);
            amenitiesResponse = modelMapper.map(newAmenitiesEntity, AmenityResponse.class);
        } else {
            reservationFee = "null";
        }
        return amenitiesResponse.toString() + "\n" + "Fee for " + amenity + "=" + reservationFee;
    }
*/