package com.example.amenitiesManagement.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenityResponse {
    private String id;
    private String name;
    private String amenitiesTypes;
    private Date date;
    private String amenityTime;
    private String userGender;
    private Integer age;

}
