package com.example.amenitiesManagement.model.request;

import com.example.amenitiesManagement.constantAndEnum.AmenityTypeEnum;
import com.example.amenitiesManagement.constantAndEnum.GenderEnum;
import com.example.amenitiesManagement.constantAndEnum.TimeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.sql.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenityRequest {

    @NotNull
    @Size(min = 3, message = "Username size not less than 3")
    @Size(max = 20, message = "Username size not greater than 20")
    @Pattern(regexp="^[a-zA-Z]+$",message="OnlyEnglishCharAllowedException")
    private String name;
    @NotNull
    private AmenityTypeEnum amenitiesTypes;
    @NotNull
    @NotNull(message = "Date Field is Mandatory")
    private Date date;
    @NotNull
    private TimeEnum amenityTime;
    @NotNull
    private GenderEnum userGender;
    @NotNull
    @Min(3)
    @Max(60)
    private Integer age;
}
