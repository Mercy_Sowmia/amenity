package com.example.amenitiesManagement.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "amenitiesManagement")
public class AmenityEntity {
    @Id
    private String id;
    private String name;
    private String amenitiesTypes;
    private Date date;
    private String amenityTime;
    private String userGender;
    private Integer age;


}
