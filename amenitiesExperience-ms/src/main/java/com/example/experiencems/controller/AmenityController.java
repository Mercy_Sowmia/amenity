package com.example.experiencems.controller;

import com.example.experiencems.constantAndEnum.AmenityTypeEnum;
import com.example.experiencems.constantAndEnum.GenderEnum;
import com.example.experiencems.constantAndEnum.TimeEnum;
import com.example.experiencems.service.AmenityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

@RestController
@RequestMapping("/management")
public class AmenityController {

    @Autowired
    AmenityService service;

    @PostMapping("/create")
    public ResponseEntity<?> addExperience(@RequestParam String name, @RequestParam AmenityTypeEnum amenityTypeEnum,
                                                @RequestParam Date date, @RequestParam TimeEnum timeEnum,
                                                @RequestParam GenderEnum genderEnum, @RequestParam Integer age) {
        return new ResponseEntity(service.create(name, amenityTypeEnum, date, timeEnum, genderEnum, age), HttpStatus.OK);
    }

    @GetMapping("/get")
    public ResponseEntity<?> getExperience(@RequestParam String id){
        return new ResponseEntity(service.get(id),HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateManagement(@RequestParam String id , @RequestParam String name,
                                              @RequestParam AmenityTypeEnum amenitiesType, @RequestParam Date date,
                                              @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender,
                                              @RequestParam Integer age){
        return new ResponseEntity(service.update(id, name, amenitiesType, date, amenityTime, gender,
                age),HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteManagement(@RequestParam String id){
        return new ResponseEntity(service.delete(id),HttpStatus.OK);
    }

}