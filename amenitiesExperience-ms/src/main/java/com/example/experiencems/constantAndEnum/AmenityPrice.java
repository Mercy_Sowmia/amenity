package com.example.experiencems.constantAndEnum;

public class AmenityPrice {


    public static final String GYM_SCHEDULE_ONE_FEE = "30 AED";
    public static final String GYM_SCHEDULE_TWO_FEE = "50 AED";

    public static final String SWIMMING_POOL_SCHEDULE_ONE_FEE = "20 AED";
    public static final String SWIMMING_POOL_SCHEDULE_TWO_FEE = "10 AED";

    public static final String STEAM_BATH_SCHEDULE_ONE_FEE = "25 AED";
    public static final String STEAM_BATH_SCHEDULE_TWO_FEE = "50 AED";

    public static final String YOGA_SCHEDULE_FEE = "50 AED";

}
