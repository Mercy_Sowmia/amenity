package com.example.experiencems.service;

import com.example.experiencems.constantAndEnum.AmenityTypeEnum;
import com.example.experiencems.constantAndEnum.GenderEnum;
import com.example.experiencems.constantAndEnum.TimeEnum;
import com.example.experiencems.model.response.AmenityResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@FeignClient(name = "amenity-reservation-process", url = "${config.rest.service.addAmenitiesManagementUrl}")
public interface AmenityInterface {

    @PostMapping("/create")
    public String createAmenityReservation(@RequestParam String name,
                                           @RequestParam AmenityTypeEnum amenityTypeEnum, @RequestParam Date date,
                                           @RequestParam TimeEnum timeEnum, @RequestParam GenderEnum genderEnum,
                                           @RequestParam Integer age);

    @GetMapping("/get")
    public AmenityResponse getAmenityReservation(@RequestParam String id);

    @PutMapping("/update")
    public String updateAmenityReservation(@RequestParam String id , @RequestParam String name,
                                           @RequestParam AmenityTypeEnum amenitiesType, @RequestParam java.sql.Date date,
                                           @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender,
                                           @RequestParam Integer age);

    @DeleteMapping("/delete")
    public String deleteAmenityReservation(@RequestParam String id);




}
