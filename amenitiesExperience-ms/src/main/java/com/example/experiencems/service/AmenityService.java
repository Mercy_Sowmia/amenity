package com.example.experiencems.service;

import com.example.experiencems.constantAndEnum.AmenityTypeEnum;
import com.example.experiencems.constantAndEnum.GenderEnum;
import com.example.experiencems.constantAndEnum.TimeEnum;
import com.example.experiencems.model.response.AmenityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class AmenityService {

    @Autowired
    AmenityInterface amenityInterface;

    //POST
    public Object create(String name, AmenityTypeEnum amenityTypeEnum, Date date, TimeEnum timeEnum, GenderEnum genderEnum,
                      Integer age) {
        return amenityInterface.createAmenityReservation(name, amenityTypeEnum, date, timeEnum, genderEnum, age);
    }

    //GET
    public AmenityResponse get(String id) {
        return amenityInterface.getAmenityReservation(id);
    }

    //PUT
    public String update(String id, String name, AmenityTypeEnum amenitiesType, Date date, TimeEnum amenityTime,
                         GenderEnum gender, Integer age) {
        return amenityInterface.updateAmenityReservation(id, name, amenitiesType, date, amenityTime, gender,
                age);
    }

    //DELETE
    public String delete(String id)
    {
        return amenityInterface.deleteAmenityReservation(id);
    }


}
